const express = require('express')
const morgan = require('morgan')
const mongoose = require('mongoose')
require('dotenv').config()

const readerRoutes = require('./routes/reader')

const app = express()
app.use(express.json())
const NODE_ENV = process.env.NODE_ENV
if (NODE_ENV === 'dev') {
  app.use(morgan('dev'))
}

app.use('/file', express.static(__dirname + '/' + process.env.FILES))
app.use('/reader', readerRoutes)

const PORT = process.env.PORT || 3000
const MESSAGE = `Server CONNECTED running on ${NODE_ENV} mode port on ${PORT}`

mongoose.connect(
  process.env.DATABASE_URI,
  { useNewUrlParser: true, useUnifiedTopology: true },
  (error) => {
    if (error) {
      console.log('***Mongo connection error:', error)
      return
    }
    app.listen(PORT, console.log(MESSAGE))
  }
)
