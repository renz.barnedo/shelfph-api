const mongoose = require('mongoose')
const Schema = mongoose.Schema

const readerSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  books: {
    type: Array,
  },
  isDeleted: {
    type: Boolean,
    required: true,
  }
})

module.exports = mongoose.model('Reader', readerSchema)
