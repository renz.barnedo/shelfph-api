const mongoose = require('mongoose')
const Schema = mongoose.Schema

const bookSchema = new Schema({
  authorId: {
    type: String,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  author: {
    type: String,
    required: true,
  },
  autoresponder: {
    name: {
      type: String,
      required: true,
    },
    list: {
      type: String,
      required: true,
    }
  },
  isDeleted: {
    type: Boolean,
    required: true,
  }
})

module.exports = mongoose.model('Book', bookSchema)
