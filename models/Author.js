const mongoose = require('mongoose')
const Schema = mongoose.Schema

const authorSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  alias: {
    type: String,
    required: true,
  },
  isDeleted: {
    type: Boolean,
    required: true,
  }
})

module.exports = mongoose.model('Author', authorSchema)
