const jwt = require('jsonwebtoken')

exports.authorize = (req, res, next) => {
  const authHeader = req.get('Authorization')
  if (!authHeader) {
    res.status(400).json({ message: 'no authorization' })
    return
  }
  const token = authHeader.split(' ')[1]
  let decodedToken
  try {
    decodedToken = jwt.verify(token, process.env.TOKEN)
  } catch (error) {
    if (error.message === 'jwt expired') {
      res.status(500).json({ message: 'Session expired. Login again.' })
      return
    }
  }
  if (!decodedToken) {
    res.status(500).json({ message: 'not authenticated.' })
    return
  }
  next()
}
