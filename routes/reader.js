const express = require('express')
const router = express.Router()

const {
  createReader,
  downloadFile,
  hostFile,
} = require('../controllers/reader')
const { login, logout } = require('../controllers/login')
const { authorize } = require('../middleware/authorize')

router.get('/purchase', createReader)
router.post('/login', login)
router.post('/logout', authorize, logout)
router.post('/download', authorize, downloadFile)
router.post('/file', authorize, hostFile)

module.exports = router
