const Author = require('../models/Author')
const Book = require('../models/Book')
const Reader = require('../models/Reader')

const moment = require('moment-timezone')
const generator = require('generate-password')
const nodemailer = require('nodemailer')
const bcrypt = require('bcryptjs')
const path = require('path')

const SALT_ROUNDS = 12
const DATETIME_FORMAT = 'YYYY-MM-DD hh:mm:ss A'

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: process.env.GMAIL_USERNAME,
    pass: process.env.GMAIL_PASSWORD,
  },
})

const autoresponders = {
  getresponse: {
    mode: 'subscribe',
  },
}

const validateUserAgent = (userAgent) =>
  Object.keys(autoresponders).includes(userAgent)
    ? autoresponders[userAgent]
    : false

exports.createReader = async (req, res, next) => {
  try {
    const userAgent = req.headers['user-agent'].toLowerCase()
    let list = '',
      mode = '',
      authorEmail = '',
      readerEmail = ''

    const autoresponder = validateUserAgent(userAgent)
    if (!autoresponder) {
      res.json({ message: 'cannot process' })
      return
    } else if (userAgent === 'getresponse') {
      const { campaign_name, action, account_login, contact_email } = req.query
      list = campaign_name
      mode = action
      authorEmail = account_login
      readerEmail = contact_email
      if (!list || !mode || !authorEmail || !readerEmail) {
        res.json({ message: 'sorry' })
        return
      }
    }

    if (autoresponder.mode !== mode) {
      res.json({ message: 'wrong setup' })
      return
    }

    let query = { email: authorEmail, isDeleted: false }
    let keys = 'id email'
    const author = await Author.findOne(query, keys).lean().exec()
    if (!author) {
      res.json({ message: 'nothing' })
      return
    }

    query = {
      authorId: author._id,
      autoresponder: {
        name: userAgent,
        list,
      },
    }
    keys = 'id autoresponder'
    const book = await Book.findOne(query, keys).lean().exec()
    console.log(query, book)
    if (!book) {
      res.json({ message: 'not existing' })
      return
    }

    const newlyBoughtBook = {
      id: book._id,
      subscriptionData: req.query,
      dateBought: moment().tz('Asia/Manila').format(DATETIME_FORMAT),
      inUse: false,
    }

    query = { email: readerEmail }
    keys = 'email books'
    const oldReader = await Reader.findOne(query, keys)

    if (oldReader) {
      const allBoughtBooks = [newlyBoughtBook, ...oldReader.books]
      query = { _id: oldReader._id }
      keys = { books: allBoughtBooks }
      const update = await Reader.updateOne(query, keys)
      if (!update) {
        res.json({ message: 'failed update' })
        return
      }
      sendAnEmail(req.query, allBoughtBooks, 'update')
      res.json({ message: 'ok2' })
      return
    }

    const password = generator.generate({
      length: 10,
      numbers: true,
      uppercase: false,
      strict: true,
    })

    const reader = new Reader({
      email: readerEmail,
      password: bcrypt.hashSync(password, SALT_ROUNDS),
      books: [newlyBoughtBook],
      isDeleted: false,
    })

    const saveReader = await reader.save()
    if (!saveReader) {
      res.json({ message: 'cannot do' })
      return
    }
    sendAnEmail(req.query, { password }, 'insert')
    res.json({ message: 'ok' })
  } catch (error) {
    console.log(error)
    res.json({ message: 'error create' })
  }
}

exports.hostFile = async (req, res) => {
  try {
    const id = req.body.id
    const type = req.body.type
    const root = path.join(__dirname, '../')
    const file = `${root}/assets/files/${id}.${type}`
    // express.static(file)
  } catch (error) {
    console.log(error)
    res.json({ message: 'error displaying' })
  }
}

exports.downloadFile = (req, res) => {
  try {
    const id = req.body.id
    const type = req.body.type
    const root = path.join(__dirname, '../')
    const file = `${root}/assets/files/${id}.${type}`
    res.download(file)
  } catch (error) {
    console.log(error)
    res.json({ message: 'error sending' })
  }
}

const sendAnEmail = async (fromAutoresponder, newData, action) => {
  if (action === 'update') {
    try {
      const { account_login, contact_email, contact_name } = fromAutoresponder
      await transporter.sendMail({
        from: process.env.GMAIL_USERNAME,
        to: contact_email,
        subject: `The book was added to your account, ${contact_name || 'reader!'}.`,
        text: `The book has been added to your account.`,
        html: `
          <span style="margin-bottom: 10px">Book has been added to your account: ${contact_email}.</span><br>
          <span style="margin-bottom: 10px">On behalf of ${account_login}, thank you!</span><br>
          <span style="margin-bottom: 10px">Forgot password? Reply to this email to request a new one.</span><br>
          `,
      })
    } catch (error) {
      console.log(error)
      return
    }
    return
  }
  const { password } = newData
  const { account_login, contact_email, contact_name } = fromAutoresponder
  try {
    await transporter.sendMail({
      from: process.env.GMAIL_USERNAME,
      to: contact_email,
      subject: `Your ShelfPH Login Details, ${contact_name || 'reader!'}.`,
      text: `Log in to the app with this Email: ${contact_email} Password: ${password} to read your book!`,
      html: `
        <span style="margin-bottom: 10px">To read your book, log in to the mobile app with this</span><br>
        Email: <strong>${contact_email}</strong><br>
        Password: <strong>${password}</strong><br>
        <div style="margin-bottom: 10px"></div>
        <span>On behalf of ${account_login}, thank you!</span><br>
      `,
    })
  } catch (error) {
    console.log(error)
  }
}
