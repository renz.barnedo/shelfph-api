const Reader = require('../models/Reader')
const Book = require('../models/Book')
const bcrypt = require('bcryptjs')
const moment = require('moment-timezone')
const jwt = require('jsonwebtoken')

const DATETIME_FORMAT = 'YYYY-MM-DD hh:mm:ss A'

exports.login = async (req, res, next) => {
  try {
    const { email, password, device } = req.body
    const reader = await Reader.findOne({
      email,
    })
      .lean()
      .exec()
    if (!reader) {
      res.status(500).json({ message: 'cannot find user' })
      return
    }

    const passwordsMatched = bcrypt.compareSync(password, reader.password)
    if (!passwordsMatched) {
      res.status(500).json({ message: 'wrong password' })
      return
    }

    let keys = 'title author'
    const readerUpdatedBooks = []
    let books = await Promise.all(
      reader.books.map(async (book) => {
        let result = await Book.findById(book.id, keys).lean().exec()
        readerUpdatedBooks.push({ ...book, inUse: true, device })
        if (book.inUse) {
          return {
            title: result.title,
            author: result.author,
            inUse: book.inUse,
          }
        }
        return {
          id: book.id,
          boughtAt: book.dateBought,
          title: result.title,
          author: result.author,
        }
      })
    )
    if (!books.length) {
      res.status(500).json({ message: 'Cannot find books' })
      return
    }

    const query = { _id: reader._id }
    keys = { books: readerUpdatedBooks }
    await Reader.updateOne(query, keys)
    const token = jwt.sign(
      {
        userId: reader._id,
        email: reader.email,
      },
      process.env.TOKEN,
      { expiresIn: '1 hour' }
      // { expiresIn: '9999 years' }
    )
    const data = { email: reader.email, books, token }
    res.json({ message: 'success', data })
  } catch (error) {
    console.log(error)
    res.status(500).json({ message: 'Cannot log in' })
  }
}

exports.logout = async (req, res, next) => {
  try {
    const { email } = req.body
    const reader = await Reader.findOne({ email: email }).lean().exec()
    if (!reader) {
      res.status(500).json({ message: 'cannot find user' })
      return
    }
    const query = { _id: reader._id }
    const books = reader.books.map((book) => ({ ...book, inUse: false }))
    const keys = { books }
    await Reader.updateOne(query, keys)
    res.status(200).json({ message: `logged out ${reader.email}` })
  } catch (error) {
    console.log(error)
    res.status(500).json({ message: 'Cannot log out' })
  }
}
